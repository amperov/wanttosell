import http from 'k6/http';

export default function () {
    const params = {
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const payload = JSON.stringify({
        firstname: 'alex',
        lastname: 'amperov',
        username: `amperov${Math.random()}`,
        email: `amperov.${Math.random()}@gmail.com`,
        password: `AmperovIsSad${Math.random()}`,
    });
    const url = 'https://wanttosell.online/api/auth/sign-up';
    http.post(url, payload, params);
}
export const options = {
    discardResponseBodies: true,
    scenarios: {
        contacts: {
            executor: 'ramping-vus',
            startVUs: 0,
            stages: [
                { duration: '1m', target: 100 },
                { duration: '10s', target: 0 },
            ],
            gracefulRampDown: '0s',
        },
    },
};