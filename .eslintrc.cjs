module.exports = {
  env: { browser: true, es2020: true },
  extends: [
    'plugin:react/jsx-runtime',
    'plugin:react-hooks/recommended',
  ],
  parserOptions: { ecmaVersion: 'latest', sourceType: 'module' },
  settings: { react: { version: '18.2' } },
  plugins: ['react-refresh'],
  rules: {
    "max-len": [
      "warn",
      {
        "code": 120,
        "ignoreComments": true
      }
    ],
    "no-console": "warn",
    "quotes": ["error", "single"],
    "semi": ["error", "always"],
  }

}
