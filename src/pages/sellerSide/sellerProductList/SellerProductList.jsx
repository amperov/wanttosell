import Header from '../../../components/common/header/Header';
import {useMediaQuery} from 'react-responsive';
import styles from './SellerProductList.module.scss';
import {useEffect, useState} from 'react';
import getClasses from '../../../utils/common/getClasses';
import MiniProduct from '../../../components/sellerPanel/miniProduct/miniProduct';
import AddProductButton from '../../../components/sellerPanel/addProductButton/AddProductButton';
import getOwnProducts from '../../../utils/api/requests/seller/product/getOwnProducts';
const SellerProductList = () => {

    const isDesktop = useMediaQuery({minWidth:1384});
    const isTablet = useMediaQuery({minWidth: 768, maxWidth:1384});
    const isMobile = useMediaQuery({maxWidth: 768});

    const [showModal, setShowModal] = useState(false);

    const changeVisibleModal = () => {
        setShowModal(!showModal);
    };

    const [products, setProducts] = useState([]);
    useEffect(() => {
        getOwnProducts().then(res => setProducts(res));
    }, []);


    return (
        <div  className={getClasses([
            [styles.containerDesktop, isDesktop],
            [styles.containerTablet, isTablet],
            [styles.containerMobile,isMobile]])}>
            <Header />

            <div style={{display:'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: 10, marginBottom: 10}}>
                <p style={{fontSize: 16, color: 'white'}}>Товары:</p>
                <AddProductButton setShowModal={changeVisibleModal} showModal={showModal}/>
            </div>
            <div>
                {!!products  && products.map(product => <MiniProduct key={product.id} product={product} />)}
            </div>
        </div>
    );
};
export default SellerProductList;