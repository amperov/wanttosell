import Header from '../../../components/common/header/Header';
import {Footer} from '../../../components/common/footer/Footer';
import {MainVariant} from '../../../components/sellerPanel/mainVariant/MainVariant';
import {useEffect, useState} from 'react';
import {GetVariantID} from '../../../utils/api/requests/seller/variant/getVariantID';
import {useParams} from 'react-router';
import {MiniItem} from '../../../components/sellerPanel/miniItem/MiniItem';
import {GetItems} from '../../../utils/api/requests/seller/getItems';
import {Text} from '../../../components/uiKit/text/Text';
import FilledButton from '../../../components/uiKit/buttons/filledButton/FilledButton';
import getClasses from '../../../utils/common/getClasses';
import styles from './SellerVariant.module.scss';
import {useMediaQuery} from 'react-responsive';
import ModalLayout from '../../../components/uiKit/modalLayout/ModalLayout';
import AddKeyModal from '../../../components/modals/addKeyModal/AddKeyModal';

export const SellerVariant = () => {
    const [variant, setVariant] = useState({});
    const [items, setItems] = useState([]);

    const [isShowModal, setShowModal] = useState(false);

    const isDesktop = useMediaQuery({minWidth:1384});
    const isTablet = useMediaQuery({minWidth: 768, maxWidth:1384});
    const isMobile = useMediaQuery({maxWidth: 768});

    const Params = useParams();
    const ProductID = Params.product_id;
    const VariantID = Params.variant_id;

    useEffect( () => {
        GetVariantID(ProductID,VariantID).then(res => setVariant(res));

        }, []);
    useEffect( () => {
        GetItems(ProductID,VariantID).then(res => setItems(res));
    }, []);

    const showModal = () =>{
        setShowModal(!isShowModal);
    };
    const successCreate = () => {
        GetItems(ProductID,VariantID).then(res => setItems(res));
    };

    return (
        <div   className={getClasses([
            [styles.containerDesktop, isDesktop],
            [styles.containerTablet, isTablet],
            [styles.containerMobile,isMobile]])}>

                <Header />

                <MainVariant variant={variant} countItems={items.length}/>

                <div style={{justifyContent: 'space-between', display:'flex', marginTop:15, marginBottom:15}}>
                    <Text fs={20}>
                        Ключи
                    </Text>

                    <FilledButton onCLick={showModal}>
                        Добавить
                    </FilledButton>
                </div>

                <div style={{background: 'white', padding: 12, borderRadius: 8}}>
                    {!!items && items.map(item => <MiniItem item={item} key={item.uuid} varID={VariantID} ProdID={ProductID}/>)}
                </div>
                <div style={{position:'fixed', bottom: 0, left: 0}}>
                    <Footer />
                </div>

            {
                isShowModal &&
                <ModalLayout handleClose={showModal}>
                    <AddKeyModal handleClose={showModal} prodID={ProductID} varID={VariantID}/>
                </ModalLayout>
            }

        </div>
    );
};