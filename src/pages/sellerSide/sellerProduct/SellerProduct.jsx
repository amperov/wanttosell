import getClasses from '../../../utils/common/getClasses';
import styles from './SellerProduct.module.scss';
import {useMediaQuery} from 'react-responsive';
import Header from '../../../components/common/header/Header';
import {useParams} from 'react-router';
import {MainProduct} from '../../../components/sellerPanel/mainProduct/MainProduct';
import {useEffect, useState} from 'react';
import {GetProductByID} from '../../../utils/api/requests/seller/product/getProductByID';
import {GetVariantsByProductID} from '../../../utils/api/requests/seller/variant/getVariantsByProductID';
import {MiniVariant} from '../../../components/sellerPanel/miniVariant/MiniVariant';
import OutlinedButton from '../../../components/uiKit/buttons/outlinedButton/OutlinedButton';
import {Footer} from '../../../components/common/footer/Footer';
import FilledButton from '../../../components/uiKit/buttons/filledButton/FilledButton';
import Dropdown from '../../../components/uiKit/dropdown/Dropdown';
import {MiniSale} from '../../../components/sellerPanel/miniSale/MiniSale';
import {MiniReview} from '../../../components/sellerPanel/miniReview/MiniReview';
import {MiniDescription} from '../../../components/sellerPanel/miniDescription/MiniDescription';

export const SellerProduct = () => {
    const [Prod, SetProd] = useState({});
    const [variants, setVariants] = useState([]);

    const [showVariants, setShowVariants] = useState(true);
    const [showSales, setShowSales] = useState(false);
    const [showReviews, setShowReviews] = useState(false);
    const [showDescriptions, setShowDescriptions] = useState(false);

    const isDesktop = useMediaQuery({minWidth:1384});
    const isTablet = useMediaQuery({minWidth: 768, maxWidth:1384});
    const isMobile = useMediaQuery({maxWidth: 768});

    const params = useParams();

    const ProductID = params.product_id;

    useEffect(() => {
         GetProductByID(ProductID).then(res => SetProd(res));
    }, []);

    useEffect(() => {
        GetVariantsByProductID(ProductID).then(res => setVariants(res));
    }, []);

    const onCreate = () => {
        GetVariantsByProductID(ProductID).then(res => setVariants(res));
    };
    return (
        <div>
            <div className={getClasses([
                [styles.containerDesktop, isDesktop],
                [styles.containerTablet, isTablet],
                [styles.containerMobile,isMobile]])}>

                <Header />

                <MainProduct product={Prod} onCreate={onCreate}/>

                {isDesktop ?

                    <div style={{
                        display: 'flex',
                        justifyContent:'center',
                        marginTop: 28, marginBottom: 28,
                        position: 'relative'}}>

                        <div style={{display: 'flex',
                            justifyContent: 'space-around', width: '50%'}}>
                            <button style={{fontSize: 24, background: 'none', border: 'none'}} onClick={()=> {
                                setShowVariants(false);
                                setShowSales(false);
                                setShowReviews(false);
                                setShowDescriptions(true);
                            }}>
                                <p style={showDescriptions ? {color: '#58B3F6'}:{color: '#6C5CD3'}}>Описание</p>
                            </button>
                            <button style={{fontSize: 24, background: 'none', border: 'none'}} onClick={()=> {
                                setShowVariants(true);
                                setShowSales(false);
                                setShowReviews(false);
                                setShowDescriptions(false);
                            }}>
                                <p style={showVariants ? {color: '#58B3F6'}:{color: '#6C5CD3'}}>Варианты</p>
                            </button>
                            <button style={{fontSize: 24, background: 'none', border: 'none'}} onClick={()=> {
                                setShowVariants(false);
                                setShowSales(true);
                                setShowReviews(false);
                                setShowDescriptions(false);
                            }}>
                                <p style={showSales ? {color: '#58B3F6'}:{color: '#6C5CD3'}}>
                                    Продажи
                                </p>
                            </button>
                            <button style={{fontSize: 24, background: 'none', border: 'none'}} onClick={()=> {
                                setShowVariants(false);
                                setShowSales(false);
                                setShowReviews(true);
                                setShowDescriptions(false);
                            }}>
                                <p style={showReviews ? {color: '#58B3F6'}:{color: '#6C5CD3'}}>
                                    Отзывы
                                </p>
                            </button>
                        </div>
                    </div>
                    :
                    <div className={styles.actionsContainer_mobile}>
                            <OutlinedButton>{
                                showVariants ? `Варианты` :
                                    showReviews ? `Отзывы` :  'Продажи'}</OutlinedButton>
                            <div className={styles.dropdown_mobile}>
                                <button style={
                                    {
                                        height: 30,
                                        borderTopLeftRadius: 12,
                                        border: 'none',
                                        borderBottom: '1px solid black',
                                        background:'white'
                                    }
                                }
                                        onClick={()=> {
                                            setShowVariants(true);
                                            setShowSales(false);
                                            setShowReviews(false);
                                            setShowDescriptions(false);
                                        }}>
                                    Варианты
                                </button>
                                <button style={
                                    {
                                        height: 30,
                                        border: 'none',
                                        borderBottom: '1px solid black'
                                    }
                                }
                                        onClick={()=> {
                                            setShowVariants(false);
                                            setShowSales(true);
                                            setShowReviews(false);
                                            setShowDescriptions(false);
                                        }}>
                                    Продажи
                                </button>
                                <button style={
                                    {
                                        height: 30,
                                        border: 'none',
                                        borderBottomLeftRadius: 12,
                                        borderBottomRightRadius: 12
                                    }
                                } onClick={()=> {
                                    setShowVariants(false);
                                    setShowSales(false);
                                    setShowReviews(true);
                                    setShowDescriptions(false);
                                }}>
                                    Отзывы
                                </button>
                            </div>

                    </div>
                }


                <div className={styles.variants}>
                    {showVariants && !!variants &&
                        variants.map(variant => <MiniVariant key={variant.id} variant={variant} productID={Prod.id}/>)}
                    {showSales && <MiniSale sale={{}}/>}
                    {showReviews && <MiniReview review={{}} /> }
                    {showDescriptions && <MiniDescription /> }
                </div>

            </div>
            <div style={{position:'fixed', bottom: 0, left: 0}}>
                <Footer />
            </div>

    </div>
    );
};