import {useMediaQuery} from 'react-responsive';
import getClasses from '../../utils/common/getClasses';
import styles from './ClientProduct.module.scss';
import Header from '../../components/common/header/Header';
import {Footer} from '../../components/common/footer/Footer';
import {MainBlock} from '../../components/clientPanel/mainBlock/MainBlock';
import {useEffect, useState} from 'react';
import {GetProductByID} from '../../utils/api/requests/seller/product/getProductByID';
import {useParams} from 'react-router';
import {GetVariantsByProductID} from '../../utils/api/requests/seller/variant/getVariantsByProductID';
import {OrderBlock} from '../../components/clientPanel/orderBlock/OrderBlock';
import {useSelector} from 'react-redux';
import {Chat} from '../../components/common/chat/Chat';

export const ClientProduct = () => {
    let params = useParams();
    const productID = params.product_id;

    const [product, setProduct] = useState({});

    useEffect(() => {
        GetProductByID(productID).then(res => setProduct(res));
    }, []);

    const isDesktop = useMediaQuery({minWidth:1384});
    const isTablet = useMediaQuery({minWidth: 768, maxWidth:1384});
    const isMobile = useMediaQuery({maxWidth: 768});


    return (
        <div   className={getClasses([
            [styles.containerDesktop, isDesktop],
            [styles.containerTablet, isTablet],
            [styles.containerMobile,isMobile]])}>
            <Header />

            <div style={{display: 'flex', justifyContent: 'space-between'}}>
                <div>
                    <MainBlock product={product}/>
                </div>
                <div>
                    <div style={{marginBottom: 20}}>
                        <Chat productID={productID}/>
                    </div>
                    <OrderBlock productID={productID} />
                </div>
            </div>

            <div style={{position:'fixed', bottom: 0, left: 0}}>
                <Footer />
            </div>
        </div>
    );
};