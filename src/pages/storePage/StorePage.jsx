import Header from '../../components/common/header/Header.jsx';
import {useEffect, useState} from 'react';
import getProducts from '../../utils/api/requests/store/getProducts.js';

import styles from './StorePage.module.scss';
import {useMediaQuery} from 'react-responsive';
import getClasses from '../../utils/common/getClasses.js';
import SliderProducts from '../../components/clientPanel/sliderProducts/sliderProducts';
import Input from '../../components/uiKit/input/Input';
import MiniProduct from '../../components/clientPanel/miniProduct/MiniProduct';
import {useSelector} from 'react-redux';


const StorePage = () => {
    const [products, setProducts] = useState([]);

    const isDesktop = useMediaQuery({minWidth:1384});
    const isTablet = useMediaQuery({minWidth: 768, maxWidth:1384});
    const isMobile = useMediaQuery({maxWidth: 768});

    useEffect(() => {
        getProducts().then(res => setProducts(res));

    }, []);



  return (
      <div className={getClasses([
          [styles.containerDesktop, isDesktop],
          [styles.containerTablet, isTablet],
          [styles.containerMobile,isMobile]])}>
          <Header/>
          <div className={getClasses([
              [styles.listDesktop, isDesktop],
              [styles.listTablet, isTablet],
              [styles.listMobile, isMobile]])}>

              <div className={styles.Slider}><SliderProducts /></div>
              {
                  isMobile && <div className={styles.Search}><Input width={340} placeholder="Поиск"/></div>
              }
              {products.map( product => <MiniProduct key={product.id} product={product}/>)}
          </div>


      </div>
  );
};
export default StorePage;