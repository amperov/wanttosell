import Header from '../../modules/Header/Header.jsx';
import {useEffect, useState} from 'react';
import storage from '../../utils/api/utils/storage.js';
import LoginButton from '../../components/LoginButton/LoginButton.jsx';
import Dropdown from '../../components/UIKit/Dropdown/Dropdown.jsx';
import SellerMenu from '../../components/menus/SellerMenu/SellerMenu.jsx';
import getProducts from '../../utils/api/requests/store/getProducts.js';
import Catalog from '../../modules/Catalog/Catalog.jsx';
import styles from './StorePage.module.scss';
import {useMediaQuery} from 'react-responsive';
import getClasses from '../../utils/common/getClasses.js';


const StorePage = () => {
    let [isAuthed , setIsAuthed] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const [products, setProducts] = useState([]);

    const isDesktop = useMediaQuery({minWidth:1368});
    const isTablet = useMediaQuery({minWidth: 768, maxWidth:1368});
    const isMobile = useMediaQuery({maxWidth: 768});

    useEffect(() => {
        getProducts().then(res => setProducts(res));
    }, []);
    useEffect(
        () => {
            setIsAuthed(!!storage.GET('access'));
        }, []);
    const authSuccessCallback = () => {
        setIsAuthed(true);
    };

    const changeVisibleModal = () => {
        setShowModal(!showModal);
    };

  return (
      <div className={getClasses([[styles.containerDesktop, isDesktop], [styles.containerTablet, isTablet], [styles.containerMobile,isMobile]])}>
          <Header>
              {
                  !isAuthed &&  <LoginButton success={authSuccessCallback} showModal={showModal} setShowModal={changeVisibleModal}/>
              }
              {
                  isAuthed &&
                  <Dropdown text="Меню" size="md">
                      {storage.GET('user_role') === 'seller' && <SellerMenu />}
                      {storage.GET('user_role') === 'user' && <SellerMenu />}
                  </Dropdown>
              }
          </Header>
          {!!products && <Catalog products={products} />}

      </div>
  );
};
export default StorePage;