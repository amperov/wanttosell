import FilledButton from '../Buttons/FilledButton/FilledButton.jsx';
import {useState} from 'react';
import OutlinedButton from '../Buttons/OutlinedButton/OutlinedButton.jsx';

const Dropdown = ({children, size, color = 'white', textColor = 'white', variant = 'outlined', text}) => {
    const [showMenu, setShowMenu] = useState(false);

    const changeVisibility = () =>{
        setShowMenu(!showMenu);
    };

    return (
        <div style={{position: 'relative'}}>
            {variant === 'filled' ?
                <FilledButton size={size} color={color} textColor={textColor} onCLick={changeVisibility}>
                    {text}
                </FilledButton> :
                <OutlinedButton size={size} color={color} textColor={textColor} onCLick={changeVisibility}>
                    {text}
                </OutlinedButton>}

            {
                showMenu && children
            }

        </div>
    );
};
export default Dropdown;