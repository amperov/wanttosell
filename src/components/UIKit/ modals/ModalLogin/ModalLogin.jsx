import {useState} from 'react';
import styles from './ModalLogin.module.scss';
import Input from '../../Input/Input.jsx';
import FilledButton from '../../Buttons/FilledButton/FilledButton.jsx';
import PasswordInput from '../../PasswordInput/PasswordInput.jsx';
import auth from '../../../../utils/api/requests/auth.js';

const ModalLogin = ( {handleClose, successCallback}) => {
    const [isSignUp, setIsSignUp] = useState(false);

    const [login, setLogin] = useState('');

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const toSignUp = () => {
        setIsSignUp(true);
        nullForm();
    };
    const toSignIn = () => {
        setIsSignUp(false);
        nullForm();
    };

    const handleChangePassword = (e) => {
        setPassword(e.target.value);
    };

    const nullForm = () => {
        setPassword('');
        setUsername('');
        setEmail('');
        setFirstName('');
        setLastName('');

        setLogin('');
    };

    const SignUp = async () => {
        await auth({
            firstname: firstName,
            lastname: lastName,
            email: email,
            password: password,
            username: username
        }, 'sign-up', successCallback);
        nullForm();
        handleClose();
    };
    const SignIn = async () => {
        await auth({
            login: login,
            password: password
        }, 'sign-in', successCallback);
        handleClose();
    };

    return (
        <div>
            {isSignUp ?
                <div>
                    <p className={styles.title}>Регистрация</p>
                    <div>
                        <Input placeholder="Введите Имя" value={firstName} width={320}
                               onChange={(e)=>{setFirstName(e.target.value);}}/>
                        <Input placeholder="Введите Фамилию" value={lastName} width={320}
                               onChange={(e)=>{setLastName(e.target.value);}}/>
                        <Input placeholder="Введите Username" value={username} width={320}
                               onChange={(e)=>{setUsername(e.target.value);}}/>
                        <Input placeholder="Введите Почту" value={email} width={320}
                               onChange={(e)=>{setEmail(e.target.value);}}/>
                        <PasswordInput placeholder="Введите пароль" value={password} width={320}
                                       onChange={handleChangePassword} />
                    </div>
                    <div className={styles.actions}>
                        <button className={styles.buttonSublink} onClick={toSignIn}>
                            <p className={styles.textSublink}>
                                Есть аккаунт?
                            </p>
                        </button>
                        <FilledButton size="sm" rounded="md" color="primary" onCLick={SignUp}>
                            Зарегистрироваться
                        </FilledButton>
                    </div>
                </div>
                :
                <div>
                    <p className={styles.title}>Авторизация</p>
                    <div>
                        <Input placeholder="Введите Логин" value={login} width={320}
                               onChange={(e) => {setLogin(e.target.value)}}/>
                        <PasswordInput placeholder="Введите пароль"
                                       value={password} onChange={handleChangePassword} width={320} />
                    </div>
                    <div className={styles.actions}>
                        <button className={styles.buttonSublink} onClick={toSignUp}>
                            <p className={styles.textSublink}>Нет аккаунта?</p>
                        </button>
                        <FilledButton size="md" rounded="md" color="primary" onCLick={SignIn}>
                            Авторизоваться
                        </FilledButton>
                    </div>
                </div>
            }
        </div>
    );
};
export default ModalLogin;