import Input from '../Input/Input.jsx';
import {useState} from 'react';
import {FaEye, FaEyeSlash} from 'react-icons/fa';

const PasswordInput = ({placeholder, value, onChange, width}) => {

    const [type, setType] = useState('password');
    const changeType=(e) =>{

        e.preventDefault();
        if (type === 'text'){
            setType('password');
        }else {
            setType('text');
        }
    };

    return (
        <Input placeholder={placeholder} type={type} value={value} onChange={onChange} width={width}>
            <button style={{background:'none', border: 'none'}} onClick={changeType}>
                {type === 'password' ? <FaEye  size={32}/> : <FaEyeSlash size={32}/>}
            </button>
        </Input>
    );
};
export default PasswordInput;