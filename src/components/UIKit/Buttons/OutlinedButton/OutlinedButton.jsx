import getClasses from '../../../../utils/common/getClasses.js';
import styles from './OutlinedButton.module.scss';
const OutlinedButton = ({size = 'lg', rounded = 'md', color = 'green', textColor = 'white', text, onCLick, children}) => {

    return (
        <button onClick={onCLick}
            className={getClasses(
            [
                styles.button,
                styles[`button_text__${textColor}`],
                styles[`button__${color}`],
                styles[`button__${size}`],
                styles[`button__rounded_${rounded}`]])}>
            <div style={{ display: 'flex', alignItems: 'center', marginLeft: 0}}>
                {children}
            </div>

        </button>
    );

};
export default OutlinedButton;