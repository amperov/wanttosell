import styles from './SellerMenu.module.scss';
import {Link} from 'react-router-dom';

const SellerMenu = () => {
    return (
        <div className={styles.buttonsWrapper}>
            <Link to="/seller/products"><button className={styles.firstButton}>Товары</button></Link>
            <Link to="/seller/analytic"><button className={styles.button}>Аналитика</button></Link>
            <Link to="/seller/wallet"><button className={styles.button}>Кошелек</button></Link>
            <Link to="/seller/setting"><button className={styles.button}>Настройка</button></Link>
            <Link to="/seller/analytic"><button className={styles.lastButton}>Выход</button></Link>
        </div>
    );
};
export default SellerMenu;