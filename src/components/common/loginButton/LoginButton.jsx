import OutlinedButton from '../../uiKit/buttons/outlinedButton/OutlinedButton.jsx';
import ModalLayout from '../../uiKit/modalLayout/ModalLayout.jsx';
import ModalLogin from '../../modals/ModalLogin/ModalLogin.jsx';

const LoginButton = ({showModal, setShowModal, success}) => {

    return (
        <div>
            <OutlinedButton onClick={setShowModal} textColor="white" color="white" size="lg" rounded="sm">Войти</OutlinedButton>
            {showModal &&
                <ModalLayout handleClose={setShowModal}>
                    <ModalLogin handleClose={setShowModal} successCallback={success}/>
                </ModalLayout>}
        </div>
    );
};
export default LoginButton;