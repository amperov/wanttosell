import {FaMessage, FaPaperclip, FaPaperPlane, FaPlane} from 'react-icons/fa6';
import {Text} from '../../uiKit/text/Text';

export const Chat = ({productID}) => {


    return (
        <div>
            <div style={{background: 'white', height: 40, borderTopLeftRadius: 12, borderTopRightRadius: 12, display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                <p style={{fontSize: 24, color: '#81ff71'}}>WantToSell</p>
            </div>
            <div style={{height: 422, background: 'white', borderTop: '1px solid black',borderBottom: '1px solid black'}}>

            </div>
            <div style={{background: 'white', borderBottomLeftRadius: 12, borderBottomRightRadius: 12, display: 'flex', justifyContent: 'space-around', padding: 10, height: 26, alignItems: 'center'}}>
                <input type="text" name="" id="" style={{width: 450, padding: '5px 20px', border: '2px solid blue', borderRadius: 6}} placeholder="Введите сообщение"/>
                <button style={{border: 'none', background: 'lightblue', display: 'flex', alignItems: 'center', padding:5, borderRadius: 6, width: 30, height: 30}}>
                    <FaPaperclip size={20} color="white"/>
                </button>

                <button style={{border: 'none', background: '#81ff71', display: 'flex', alignItems: 'center', padding:5, borderRadius: 6}}>
                    <FaPaperPlane size={20} color="white"/>
                </button>
            </div>
        </div>
    );
};