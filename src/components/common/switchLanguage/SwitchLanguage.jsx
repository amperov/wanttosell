import SwitchButton from '../../uiKit/buttons/switchButton/SwitchButton';
import {actions} from '../../../store/settings/settings';
import {useDispatch, useSelector} from 'react-redux';
import {useState} from 'react';
import {useMediaQuery} from 'react-responsive';

const SwitchLanguage = () => {
    const dispatch = useDispatch();
    const lang = useSelector(state => state.LangReducer.lang);
    const [toggledLang, setToggledLang] = useState('l');

    const isDesktop = useMediaQuery({minWidth: 1384});
    const toggleLanguage = () => {
        if (lang === 'en-US'){
            dispatch(actions.changeLocale('ru-RU'));
            setToggledLang('r');
        }
        if (lang === 'ru-RU'){
            //dispatch
            dispatch(actions.changeLocale('en-US'));
            setToggledLang('l');
        }
    };

    return (
            <SwitchButton lValue="En" rValue="Ru" toggled={toggledLang} onToggle={toggleLanguage}/>
    );
};

export default SwitchLanguage;