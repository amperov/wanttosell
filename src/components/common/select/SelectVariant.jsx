import {useEffect, useState} from 'react';
import {GetVariantsByProductID} from '../../../utils/api/requests/seller/variant/getVariantsByProductID';
import {useSelector} from 'react-redux';
import styles from './SelectVariant.module.scss';
import getClasses from '../../../utils/common/getClasses';
import {ArrowDown} from '../../../assets/icons';
export const SelectVariant = ({productID, onChoose}) => {

    const lang = useSelector(state => state.LangReducer.lang);
    const [chosen, setChosen] = useState(0);
    const [variants, setVariants] = useState([]);

    const [isChosen, setIsChosen] = useState(false);

    useEffect(() => {
        GetVariantsByProductID(productID).then(res => setVariants(res));
    }, []);

    const choose = (index) =>{
        setIsChosen(true);
        setChosen(index);
        onChoose(index);
    };

    return (
        <div style={{display:'block', position: 'relative'}}>
            <div style={{display:'block'}}>
                <div className={styles.select}>
                    <div style={{display:'flex', justifyContent: 'space-between'}}>
                        <p>{isChosen ? variants.find(val => val.id === chosen).title.find(val => val.lang === lang).value : `Выберите вариант`}</p>
                        <ArrowDown />
                    </div>

                    <div className={styles.menu}>
                        <div className={styles.buttonsWrapper}>
                                {variants.map((variant, index) =>
                                    <button key={variant.id}
                                            className={getClasses([
                                                [styles.button, index !== 0 && index !== variants.length],
                                                [styles.firstButton, index === 0],
                                                [styles.lastButton, index === variants.length -1]])}
                                    onClick={() => {choose(variant.id)}}
                                    >
                                        {variant.title.find(val => val.lang === lang).value}
                                    </button>)}
                            </div>
                    </div>

                </div>
            </div>
        </div>
    );
};