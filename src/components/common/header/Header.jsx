import styles from './Header.module.scss';
import Input from '../../uiKit/input/Input';
import {useMediaQuery} from 'react-responsive';
import LoginButton from '../loginButton/LoginButton';
import Dropdown from '../../uiKit/dropdown/Dropdown';
import storage from '../../../utils/api/utils/storage';
import {useEffect, useState} from 'react';
import {MenuDropdown as SellerMenu} from '../../sellerPanel/menuDropdown/MenuDropdown';
import SwitchLanguage from '../switchLanguage/SwitchLanguage';
import getClasses from '../../../utils/common/getClasses';
const Header = () => {
    const [isAuthed , setIsAuthed] = useState(false);
    const [showModal, setShowModal] = useState(false);

    const isDesktop = useMediaQuery({minWidth:1384});

    const isMobile = useMediaQuery({maxWidth: 768});
    const authSuccessCallback = () => {
        setIsAuthed(true);
    };
    const changeVisibleModal = () => {
        setShowModal(!showModal);
    };

    useEffect(
        () => {
            setIsAuthed(!!storage.GET('access'));
        }, []);

    return (
        <div className={getClasses([[styles.wrapper_desktop, isDesktop], [styles.wrapper_tablet, !isDesktop&&!isMobile], [styles.wrapper_mobile, isMobile]])}>
            <p style={{color: 'white', fontSize: 24}}>Want2Sell</p>
            {!isMobile && <Input placeholder="Поиск" width={340}/> }

            <SwitchLanguage />

            <div>

                {!isAuthed &&
                    <LoginButton success={authSuccessCallback}
                                 showModal={showModal}
                                 setShowModal={changeVisibleModal}/>}
                {isAuthed &&
                    <Dropdown text="Меню" size={isDesktop ? 'lg' : 'sm'}>
                        {storage.GET('user_role') === 'seller' && <SellerMenu />}
                        {storage.GET('user_role') === 'user' && <SellerMenu />}
                        {storage.GET('user_role') === 'admin' && <SellerMenu />}
                    </Dropdown>}
            </div>
        </div>
    );
};
export default Header;