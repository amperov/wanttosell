import {TelegramIcon, VKIcon, XIcon} from '../../../assets/icons';

export const Footer = () => {

    return (
        <div style={{ background: '#192A53', paddingTop: 15, width: '100vw'}}>
            <div style={{display:'flex', justifyContent:'center'}}>
                <div style={{display: 'flex', justifyContent: 'space-around', width: '15%', paddingBottom: 10}}>
                    <XIcon />
                    <TelegramIcon />
                    <VKIcon />
                </div>
            </div>
            <p style={{color:'white', fontSize: 16, textAlign:'center', paddingBottom: 15}}>All rights reserved</p>
        </div>

    );
};