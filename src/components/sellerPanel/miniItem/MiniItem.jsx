import {Text} from '../../uiKit/text/Text';
import FilledButton from '../../uiKit/buttons/filledButton/FilledButton';
import ModalLayout from '../../uiKit/modalLayout/ModalLayout';
import EditKeyModal from '../../modals/editKeyModal/EditKeyModal';
import {useState} from 'react';

export const  MiniItem = ({item, varID, ProdID}) => {
    const [isShowModal, setIsShowModal] = useState(false);
    return (
        <div style={{marginTop:4, marginBottom: 4, position: 'relative', paddingTop: 10, paddingBottom: 10}}>
            <Text fs={18} >
                {item.content}
            </Text>
            <div style={{position:'absolute', right: 0, top: 0, display: 'flex'}}>
                <div style={{marginRight: 20}}>
                    <FilledButton color="purple" onCLick={() => {setIsShowModal(!isShowModal);}}>
                        Изменить
                    </FilledButton>
                </div>
                <FilledButton color="error">
                    Удалить
                </FilledButton>
                {
                    isShowModal &&
                    <ModalLayout handleClose={() => {setIsShowModal(!isShowModal);}}>
                        <EditKeyModal varID={varID}
                                      prodID={ProdID} item={item}
                                      handleClose={() => {setIsShowModal(!isShowModal);}} />
                    </ModalLayout>
                }

            </div>
        </div>
    );
};