import {English, Russian} from '/src/assets/icons/index';
import {Text} from '../../uiKit/text/Text';
import styles from './miniProduct.module.scss'
import FilledButton from '../../uiKit/buttons/filledButton/FilledButton';
import {Link} from 'react-router-dom';
import {Badge} from '../../uiKit/badge/Badge';
import Dropdown from '../../uiKit/dropdown/Dropdown';
import {useMediaQuery} from 'react-responsive';
import getClasses from '../../../utils/common/getClasses';


const MiniProduct = ({product}) => {
const isDesktop = useMediaQuery({minWidth: 1384});
    const isMobile = useMediaQuery({maxWidth: 784});
    return (
        <div className={styles.wrapper}>
            <div style={{display:'flex'}}>
                {!isMobile &&
                    <img src={product.photo} alt="" className={getClasses([
                        [styles.image_desktop, isDesktop],
                        [styles.image_tablet, !isMobile&&!isDesktop]])}/>
                }

                <div className={getClasses([
                        [styles.info_container_desktop, isDesktop ],
                        [styles.info_container_tablet, !isMobile&&!isDesktop],
                        [styles.info_container_mobile, isMobile]
                ])}>

                    <div style={ isDesktop ? {marginBottom:18} : {marginBottom:20}}>
                        <div style={{marginBottom:5}}>
                            <Text prependIcon={<Russian />}>
                                {product.title.find(title => title.lang === 'ru-RU').value}
                            </Text>
                        </div>

                        <Text prependIcon={<English />}>
                            {product.title.find(title => title.lang === 'en-US').value}
                        </Text>
                    </div>



                        <div className={
                            getClasses([[styles.field_desktop, isDesktop], [styles.field_tablet, !isDesktop]])}>
                            <p>Тип:</p>
                            <div style={{marginLeft: 20}}>{product.type}</div>
                        </div>

                        <div className={
                            getClasses([[styles.field_desktop, isDesktop], [styles.field_tablet, !isDesktop]])}>
                            <p>Платформа:</p>
                            <div style={{marginLeft: 20}}>{product.platform}</div>
                        </div>

                    {/* show this info only on desktop*/}
                    {!isMobile &&
                    <div>
                        <div  className={getClasses([
                                [styles.field_desktop, isDesktop],
                            [styles.field_tablet, !isDesktop]
                        ])}>
                            <p>Вариантов:</p>
                            <div style={{marginLeft: 20}}>

                                {!!product.count_variants ? product.count_variants : 0}

                            </div>
                        </div>

                        <div  className={getClasses([
                            [styles.field_desktop, isDesktop],
                            [styles.field_tablet, !isDesktop]
                        ])}>
                            <div style={{display: 'flex'}}>
                                <p>В наличии:</p>
                                <div style={{marginLeft: 20}}>
                                    <div>
                                        {!!product.count_keys ? product.count_keys : 0}
                                    </div>
                                </div>
                            </div>
                            <div style={{display: 'flex', marginLeft: 40}}>
                                <div style={{display: 'flex'}}>
                                    <p>Продано:</p>
                                    <div style={{marginLeft: 20}}>
                                        <div>
                                            {!!product.sold ? product.sold : 0}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    }


                        <div style={{display:'flex', position: 'absolute', right:16, bottom:16}}>
                            <Dropdown size="sm"
                                            textColor="white"
                                            color="purple" text="Действия" variant="filled">
                                <div className={styles.actionsContainer}>
                                    <button className={styles.first_button}>Редактировать</button>
                                    <button className={styles.button} disabled>Отключить</button>
                                    <button className={styles.last_button}>Удалить</button>
                                </div>

                            </Dropdown>
                            <div style={{marginLeft: 16}}>
                                <Link to={`/seller/products/${product.id}`}>
                                    <FilledButton color="success" size="md" textColor="black" rounded="sm">
                                        Перейти
                                    </FilledButton>
                                </Link>
                            </div>
                        </div>
                        <div style={{color: "#2c9c9e", position: 'absolute', top: 16, right: 26}}>
                            <Badge>Включён</Badge>
                        </div>

                </div>
            </div>
        </div>


    );
};
export default MiniProduct;