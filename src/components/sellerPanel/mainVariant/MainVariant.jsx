import {Text} from '../../uiKit/text/Text';
import {English, Russian} from '../../../assets/icons';
import styles from '../miniVariant/MiniVariant.module.scss';
import FilledButton from '../../uiKit/buttons/filledButton/FilledButton';
import {Link} from 'react-router-dom';
import {useState} from 'react';
import {DeleteVariant} from '../../../utils/api/requests/seller/variant/deleteVariant';
import {useParams} from 'react-router';

export const MainVariant = ({variant, countItems}) => {
    const Params = useParams();
    const [isShowActions, setShowActions] = useState(false);
    const showActions = () => {
        setShowActions(!isShowActions);
    };

    const onDelete = () =>{
        DeleteVariant(ProductID, variant.id);
    };

    const ProductID = Params.product_id;
    return (
        <div style={{background: 'white', borderRadius: 12, position: 'relative', padding: 12}}>
            <div style={{marginBottom: 3, display: 'flex', alignItems: 'center',marginLeft: 10}}>
                <Text prependIcon={<Russian style={{scale: 0.5}}/>}>
                    {!!variant.title && variant.title.find(title => title.lang === 'ru-RU').value}
                </Text>
            </div>
            <div style={{marginBottom: 12, display: 'flex', alignItems: 'center',marginLeft: 10}}>
                <Text prependIcon={<English />}>
                    {!!variant.title && variant.title.find(title => title.lang === 'en-US').value}
                </Text>
            </div>

            <div>
                <Text fs={16}>
                    Количество ключей: {!!countItems ? countItems: 0}
                </Text>
                <Text fs={16}>
                    Продано: {!!variant.sold ? variant.sold: 0}
                </Text>
            </div>

                <div style={{display:'flex', position: 'absolute', right:14, bottom:14}}>
                    <div style={{position:'relative'}} className={styles.actions}>
                        <FilledButton size="md"
                                      textColor="white"
                                      color="purple" onCLick={showActions}>
                            Действия
                        </FilledButton>
                        <div className={styles.actionsContainer}>
                            <button className={styles.first_button}>Редактировать</button>
                            <button className={styles.last_button} onClick={onDelete}>Удалить</button>
                        </div>
                    </div>

                </div>
        </div>
    );
};