import {Text} from '../../uiKit/text/Text';
import {English, Russian} from '../../../assets/icons';

import {Link} from 'react-router-dom';
import FilledButton from '../../uiKit/buttons/filledButton/FilledButton';
import {useParams} from 'react-router';
import styles from './MiniVariant.module.scss';
import {useState} from 'react';
import {DeleteVariant} from '../../../utils/api/requests/seller/variant/deleteVariant';

export const MiniVariant = ({variant, productID}) => {
    const [isShowActions, setShowActions] = useState(false);
    const showActions = () => {
        setShowActions(!isShowActions);
    };
    const Params = useParams();
    const ProductID = Params.product_id;

    const onDelete = () =>{
        DeleteVariant(ProductID, variant.id);
    };
    return (
        <div style={{display: 'flex', background: 'white', borderRadius: 12, margin: '15px 0', position: 'relative'}}>

            <div style={{ borderTopRightRadius: 12, borderBottomRightRadius: 12, padding: 16}}>
                        <div style={{marginBottom: 3, display: 'flex', alignItems: 'center'}}>
                            <Text prependIcon={<Russian style={{scale: 0.5}}/>}>
                                {variant.title.find(title => title.lang === 'ru-RU').value}
                            </Text>
                        </div>
                <div style={{marginBottom: 12, display: 'flex', alignItems: 'center'}}>
                        <Text prependIcon={<English />}>
                            {variant.title.find(title => title.lang === 'en-US').value}
                        </Text>
                </div>
                <div style={{display:'flex', marginBottom: 10}}>
                    <p>Цена:</p>
                    <div style={{marginLeft: 20}}>
                        <div>
                            {variant.price}
                        </div>
                    </div>
                </div>

                    <div style={{display: 'flex'}}>
                        <p>Количество товара:</p>
                        <div style={{marginLeft: 20}}>
                            <div>
                                {!!variant.count_keys ? variant.count_keys : 0}
                            </div>
                        </div>
                    </div>


                <div style={{display:'flex', position: 'absolute', right:14, bottom:14}}>
                    <div style={{position:'relative'}} className={styles.actions}>
                        <FilledButton size="md"
                                      textColor="white"
                                      color="purple" onCLick={showActions}>
                                Действия
                        </FilledButton>
                        <div className={styles.actionsContainer}>
                            <Link to={`/seller/products/${productID}/variants/${variant.id}`}>
                                <button className={styles.first_button}>Перейти</button>
                            </Link>
                            <button className={styles.button}>Редактировать</button>
                            <button className={styles.last_button} onClick={onDelete}>Удалить</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    );
};