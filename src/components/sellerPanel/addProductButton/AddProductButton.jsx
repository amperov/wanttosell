import ModalLayout from '../../uiKit/modalLayout/ModalLayout.jsx';
import AddProductModal from '../../modals/addProductModal/AddProductModal';
import OutlinedButton from '../../uiKit/buttons/outlinedButton/OutlinedButton';

const AddProductButton = ({showModal, setShowModal, success}) => {

    return (
        <div>
            <OutlinedButton onClick={setShowModal} textColor="white"
                          color="success" size="lg" rounded="sm">
                Добавить товар
            </OutlinedButton>
            {showModal &&
                <ModalLayout handleClose={setShowModal}>
                    <AddProductModal handleClose={setShowModal}/>
                </ModalLayout>}
        </div>
    );
};
export default AddProductButton;