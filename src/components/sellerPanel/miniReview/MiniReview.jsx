import styles from './MiniReview.module.scss';
import {Text} from '../../uiKit/text/Text';
import {Badge} from '../../uiKit/badge/Badge';
import FilledButton from '../../uiKit/buttons/filledButton/FilledButton';
export const MiniReview = ({review}) =>{
    return (
        <div className={styles.wrapper_desktop}>
            <div style={{minWidth: 750, display: 'flex',  flexDirection:'column'}}>
                <div style={{marginBottom: 4, textAlign: 'left'}}>
                    <Text fs={24}>
                        {!!review.email ? review.email : `alex.amperov@gmail.com`}
                    </Text>
                </div>
                <div style={{marginBottom: 20}}>
                    <Text fw={500}>
                        {`${!!review.product ? `${review.product}` : 'Название продукта'} 
                        - 
                        ${!!review.variant ? `${review.variant}` : 'Название варианта'}`}
                    </Text>
                </div>
                    <p style={{color: '#7D7D7D', marginLeft: 10, fontWeight: 300}}>
                        {!!review.text ? `${review.text}` : `Нет текста`}
                    </p>
            </div>

        <div className={styles.rating}>
                {!!review.rating ? `Оценка: ${review.rating}` : `Оценка: 0`}
        </div>
        </div>
    );
};