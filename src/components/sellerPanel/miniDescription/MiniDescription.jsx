import styles from './MiniDescription.module.scss';
import {English, Russian} from '../../../assets/icons';
import OutlinedButton from '../../uiKit/buttons/outlinedButton/OutlinedButton';
export const MiniDescription = ({desc = {lang: 'ru-RU', value: 'Просто текст для описания'}}) =>{

    return (
        <div className={styles.wrapper_desktop}>
            <div>
                <p style={{fontSize: 20, color: 'white', marginLeft: 17, marginBottom: 17}}>
                    {desc.lang === 'ru-RU' ? <Russian/> : <English />} Описание:
                </p>
            </div>
            <div>
                <p style={{color: 'white', marginLeft:24}}>
                    {!!desc.value ? desc.value: `Нет описания`}
                </p>
            </div>
            <div style={{display: 'flex', justifyContent: 'end'}}>
                <OutlinedButton color="success">Изменить</OutlinedButton>
            </div>
        </div>
    );
};