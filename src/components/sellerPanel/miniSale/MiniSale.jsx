import styles from './MiniSale.module.scss';
import {Text} from '../../uiKit/text/Text';
import {Badge} from '../../uiKit/badge/Badge';
import FilledButton from '../../uiKit/buttons/filledButton/FilledButton';
export const MiniSale = ({sale}) =>{
    return (
        <div className={styles.wrapper_desktop}>
            <div style={{minWidth: 750, display: 'flex',  flexDirection:'column'}}>
                <div style={{marginBottom: 4, textAlign: 'left'}}>
                    <Text fs={16}>
                        {!!sale.email ? sale.email : `alex.amperov@gmail.com`}
                    </Text>
                </div>
                <div style={{marginBottom: 4}}>
                    <Text>
                        {`Вариант: ${!!sale.variant ? sale.variant : 'Не указан вариант'}`}
                    </Text>
                </div>
                <div style={{marginBottom: 4}}>
                    <Text>
                        {`Дата: ${!!sale.date ? sale.date : 'Не указана дата'}`}
                    </Text>
                </div>
            </div>
            <div style={{display:'flex', alignItems: 'center',
                justifyContent: 'space-between', width: '100%', marginRight: 20}}>
                <Text>
                    {`Количество: ${!!sale.count ? sale.count : '0'}`}
                </Text>
                <div style={{display:'block'}}>
                    <Badge>
                        {`${!!sale.variant ? 
                            sale.variant > 0 ? 
                                `+ ${sale.profit}` : `- ${sale.profit}` 
                            : `+ 75 000`}`}
                    </Badge>
                </div>
                <div style={{display:'block'}}>
                <FilledButton color="success" textColor="white" size="sm" rounded="sm">
                    Инфо
                </FilledButton>
                </div>
            </div>

        </div>
    );
};