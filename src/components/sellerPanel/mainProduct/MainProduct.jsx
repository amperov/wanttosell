import {Text} from '../../uiKit/text/Text';
import {Badge} from '../../uiKit/badge/Badge';
import {English, Russian} from '../../../assets/icons';
import {useState} from 'react';
import styles from './MainProduct.module.scss';
import FilledButton from '../../uiKit/buttons/filledButton/FilledButton';
import getClasses from '../../../utils/common/getClasses';
import {useMediaQuery} from 'react-responsive';
import ModalLayout from '../../uiKit/modalLayout/ModalLayout';
import AddVariantModal from '../../modals/addVariantModal/AddVariantModal';
import {DeleteProduct} from '../../../utils/api/requests/seller/product/deleteProduct';
import {EditProductModal} from '../../modals/editProductModal/EditProductModal';

export const MainProduct = ({product, onCreate}) => {
    const [showCreateVariantModal, setShowCreateVariantModal] = useState(false);
    const [showEditProductModal, setShowEditProductModal] = useState(false);

    const showCreateModal = () => {
        setShowCreateVariantModal(!showCreateVariantModal);
    };

    const showEditModal = () => {
        setShowEditProductModal(!showEditProductModal);
    };

    const onDelete = () => {
        DeleteProduct(product.id);
    };


    const isDesktop = useMediaQuery({minWidth:1384});
    const isMobile = useMediaQuery({maxWidth:786});

    return (
        <div>
            <div className={styles.wrapper}>

                {!isMobile &&
                    <img src={product.photo} alt="" className={getClasses([styles.photo])}/>
                }

                <div className={getClasses([
                    [styles.info_container_desktop, isDesktop],
                    [styles.info_container_mobile, isMobile],
                [styles.info_container_desktop, !isDesktop&&!isMobile]])}>

                    <div style={{marginBottom: 24}}>
                            <div style={{marginBottom: 5}}>
                                <Text prependIcon={<Russian />}>
                                    {!!product.title && product.title.find(title => title.lang === 'ru-RU').value}
                                </Text>
                            </div>
                            <Text prependIcon={<English />}>
                                {!!product.title && product.title.find(title => title.lang === 'en-US').value}
                            </Text>
                    </div>


                    <div className={getClasses([
                        [styles.info_field_desktop, isDesktop],
                        [styles.info_field_mobile, !isDesktop]
                    ])}>
                        <p>Тип:</p>
                        <div style={{marginLeft: 20}}>
                            <div>
                                {product.type}
                            </div>
                        </div>
                    </div>

                    <div className={getClasses([
                        [styles.info_field_desktop, isDesktop],
                        [styles.info_field_mobile, !isDesktop]
                    ])}>
                        <p>Платформа:</p>
                        <div style={{marginLeft: 20}}>
                            <div>
                                {product.platform}
                            </div>
                        </div>
                    </div>

                    {!isMobile &&
                        <div>
                            <div  className={getClasses([
                                [styles.info_field_desktop, isDesktop],
                                [styles.info_field_mobile, !isDesktop]
                            ])}>
                                <p>Вариантов:</p>
                                <div style={{marginLeft: 20}}>

                                    {!!product.count_variants ? product.count_variants : 0}

                                </div>
                            </div>

                            <div  className={getClasses([
                                [styles.info_field_desktop, isDesktop],
                                [styles.info_field_mobile, !isDesktop]
                            ])}>
                                <div style={{display: 'flex'}}>
                                    <p>В наличии:</p>
                                    <div style={{marginLeft: 20}}>
                                        <div>
                                            {!!product.count_keys ? product.count_keys : 0}
                                        </div>
                                    </div>
                                </div>
                                <div style={{display: 'flex', marginLeft: 40}}>
                                    <div style={{display: 'flex'}}>
                                        <p>Продано:</p>
                                        <div style={{marginLeft: 20}}>
                                            <div>
                                                {!!product.sold ? product.sold : 0}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    }



                    <div className={styles.actionsButton}>
                        <div className={styles.actions}>
                            <FilledButton size="sm"
                                          textColor="white"
                                          color="purple">
                                Действия
                            </FilledButton>
                            <div className={styles.actionsContainer}>
                                <button className={styles.first_button}
                                        onClick={showCreateModal}>Создать вариант</button>
                                <button className={styles.button}
                                        onClick={showEditModal}>Редактировать</button>
                                <button className={styles.button}
                                        disabled>Отключить</button>
                                <button className={styles.last_button}
                                        onClick={onDelete}>Удалить</button>
                            </div>
                        </div>
                    </div>

                    <div className={styles.status}>
                        <Badge>Включен</Badge>
                    </div>
                </div>
                {showCreateVariantModal &&
                    <ModalLayout handleClose={showCreateModal}>
                        <AddVariantModal
                            handleClose={showCreateModal}
                            prodID={product.id}/>
                    </ModalLayout>
                }
                {showEditProductModal &&
                    <ModalLayout handleClose={showEditModal}>
                        <EditProductModal
                            handleClose={showEditModal}
                            product={product}/>
                    </ModalLayout>
                }

            </div>
        </div>
    );
};
