
import {useState} from 'react';
import styles from './MiniProduct.module.scss';
import {useMediaQuery} from 'react-responsive';
import getClasses from '../../utils/common/getClasses.js';
const MiniProduct = ({product}) => {
    const [isLoaded, setIsLoaded] = useState(false);
    const isDesktop = useMediaQuery({minWidth: 1368});
    const isMobile = useMediaQuery({maxWidth:768});
    const isTablet = useMediaQuery({minWidth:768, maxWidth: 1368});
    const Load = () => {
        setIsLoaded(true);
    };
    return (
        <div className={getClasses([[styles.wrapperDesktop, isDesktop], [styles.wrapperMobile, isMobile]])}>

            <img src={product.photo}
                 className={getClasses([
                     [styles.product_photo_desktop, isDesktop],
                     [styles.product_photo_mobile, isMobile],
                    [styles.product_photo_desktop, isTablet]
                 ])} onLoad={Load}/>
            {isLoaded && <p className={getClasses([[styles.product_title_desktop, true]])}>{product.title}</p>}
        </div>
    );
};
export default MiniProduct;