import {useMediaQuery} from 'react-responsive';
import React from 'react';
import getClasses from '../../../utils/common/getClasses';
import styles from './SliderProducts.module.scss';
const  sliderProducts = () => {
    const isDesktop = useMediaQuery({minWidth: 1384});
    const isMobile = useMediaQuery({maxWidth: 768});
    const isTablet = useMediaQuery({minWidth: 768, maxWidth: 1384});

    return (
        <div className={getClasses([[styles.wrapperDesktop, isDesktop], [styles.wrapperTablet, isTablet], [styles.wrapperMobile, isMobile]])}>
                <div >
                    <p>Место под слайдер типа</p>
                </div>
        </div>
    );
};
export default sliderProducts;