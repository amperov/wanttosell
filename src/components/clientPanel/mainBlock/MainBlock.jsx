import {Badge} from '../../uiKit/badge/Badge';

export const MainBlock = ({product}) => {


    return (
        <div style={{margin: 0, padding: 0, display: 'block', width: 735}}>
            <div style={{
                display:'flex', alignItems: 'center',
                justifyContent: 'space-between', borderTopLeftRadius: 24, borderTopRightRadius: 24,
                background: '#343434', paddingLeft: 15, paddingRight: 15, height: 45, width: 735, boxSizing:'border-box'}}>
                <p style={{color: 'white'}}>Рейтинг: {!!product.rating && product.rating}</p>
                <p style={{color: 'white'}}>Продавец: {!!product.seller && product.seller}</p>
            </div>


                <img src={product.photo} alt="" style={{height:420, width: 735, marginBottom: 0, padding: 0,display:'block'}}/>

            <div style={{
                display:'flex', alignItems: 'center',
                justifyContent: 'start', borderBottomLeftRadius: 24,
                borderBottomRightRadius: 24, background: '#343434',
                paddingLeft: 15, paddingRight: 15, height: 45, width: 735, boxSizing:'border-box', marginTop:0}}>
                <Badge> {product.platform}</Badge>
                <Badge> {product.type}</Badge>
                <Badge> {product.delivery}</Badge>
            </div>
        </div>
    );
};