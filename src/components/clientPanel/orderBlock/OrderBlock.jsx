import {SelectVariant} from '../../common/select/SelectVariant';
import {useEffect, useState} from 'react';
import {IncDecInput} from '../../uiKit/IncDecInput/incDevInput';
import {GetVariantID} from '../../../utils/api/requests/seller/variant/getVariantID';
import {useSelector} from 'react-redux';
import {GetProductByID} from '../../../utils/api/requests/seller/product/getProductByID';

import {GetSummaryCost} from '../../../utils/api/requests/store/getSummaryCost';

import styles from './OrderBlock.module.scss';
import {Badge} from '../../uiKit/badge/Badge';
import FilledButton from '../../uiKit/buttons/filledButton/FilledButton';

export const OrderBlock = ({productID}) => {
    const [minValue, setMinValue] = useState(0);

    const [chosenVariant, setChosenVariant] = useState(0);
    const [count, setCount] = useState(0);
    const [product, setProduct] = useState({});
    const lang = useSelector(state => state.LangReducer.lang);

    const [pricePerOne, setPricePerOne] = useState(0.0);

    const [cost, setCost] = useState(0);

    useEffect(() => {
        GetProductByID(productID).then(res => setProduct(res));
    }, []);

    useEffect(() => {
        GetSummaryCost(pricePerOne, count).then(res => setCost(res));
        console.log(pricePerOne);
    }, [count, chosenVariant]);
    const initValuesFromRequest = (res) => {
        setMinValue(parseInt(res.min_count));
        setCount(parseInt(res.min_count));
        setPricePerOne(parseFloat(res.price));
    };
    const onSelect = (VariantID) => {
        if (VariantID !== chosenVariant){
            setChosenVariant(VariantID);
            GetVariantID(productID, VariantID).then(res => initValuesFromRequest(res));
        }
    };

    return (
        <div className={styles.wrapper}>
                <p style={{fontSize: 24, color: 'white', marginBottom: 10}}>
                    {!!product.title && product.title.find(val => val.lang === lang).value}
                </p>
            <div style={{display: 'flex', alignItems: 'center'}}>
                <p style={{marginRight: 30, fontSize: 20, color: 'white'}}>
                    Вариант:
                </p>
                <SelectVariant productID={productID} onChoose={onSelect}/>
            </div>

            <div style={{display: 'flex',alignItems: 'center', marginTop: 12, marginBottom: 10}}>
                <p style={{fontSize: 20, color: 'white',marginRight: 30}}>
                    Количество:
                </p>
                <IncDecInput maxValue={1500} minValue={minValue} value={count} onChange={(count) => setCount(count)} isChosen={!!chosenVariant}/>
            </div>
            <div style={{display: 'flex', justifyContent: 'end'}}>
                <div style={{display: 'flex', alignItems: 'center', marginRight: 40}}>
                    <p style={{ fontSize: 20, color: 'white', marginRight: 20}}>
                        Цена:
                    </p>
                    <Badge br={6}>{cost} руб</Badge>
                </div>
                <FilledButton color="success" textColor="black">Оплатить</FilledButton>
            </div>

        </div>
    );
};