
import {useState} from 'react';
import styles from './MiniProduct.module.scss';
import {useMediaQuery} from 'react-responsive';
import getClasses from '../../../utils/common/getClasses.js';
import {Link} from 'react-router-dom';
import {useSelector} from 'react-redux';
const MiniProduct = ({product}) => {
    const lang = useSelector(state => state.LangReducer.lang);
    const [isLoaded, setIsLoaded] = useState(false);
    const isDesktop = useMediaQuery({minWidth: 1368});
    const isMobile = useMediaQuery({maxWidth:768});
    const isTablet = useMediaQuery({minWidth:768, maxWidth: 1368});
    const Load = () => {
        setIsLoaded(true);
    };
    return (
        <Link to={`/store/${product.id}`} style={{textDecoration: 'none', MozWindowDragging: 'no-drag'}}>
            <div className={getClasses([[styles.wrapperDesktop, isDesktop], [styles.wrapperMobile, isMobile]])}>
                <div style={{position: 'relative'}}>
                    <img src={product.photo}
                         className={getClasses([
                             [styles.product_photo_desktop, isDesktop],
                             [styles.product_photo_mobile, isMobile],
                             [styles.product_photo_desktop, isTablet]
                         ])} onLoad={Load}/>
                    <div className={styles.onHover}>
                        <p style={{
                            color: 'white', textAlign: 'center',
                            fontSize: 20, marginTop: 10, marginBottom: 10,
                            opacity: 1}}>
                            Want2Sell
                        </p>
                        <p style={{color: 'white', marginLeft: 12, marginBottom: 2, opacity: 1}}>
                            Продаж: {product.sold || 0}
                        </p>
                        <p style={{color: 'white', marginLeft: 12, marginBottom: 2, opacity: 1}}>
                            Рейтинг: {product.rating || 0}
                        </p>
                        <p style={{color: 'white', marginLeft: 12, marginBottom: 2, opacity: 1}}>
                            Отзывы: {product.reviews || 0}
                        </p>
                    </div>
                </div>

                {isLoaded &&
                    <p className={getClasses([
                        [styles.product_title_desktop, isDesktop],
                        [styles.product_title_mobile, isMobile],
                        [styles.product_title_tablet, isTablet]
                    ])}>{product.title.find(val => val.lang === lang).value}</p>}
            </div>
        </Link>
    );
};
export default MiniProduct;