import {useState} from 'react';
import styles from './AddProductModal.module.scss';
import FilledButton from '../../uiKit/buttons/filledButton/FilledButton.jsx';

import {useMediaQuery} from 'react-responsive';
import Input from '../../uiKit/input/Input';
import OutlinedButton from '../../uiKit/buttons/outlinedButton/OutlinedButton';
import {AddSingleItem} from '../../../utils/api/requests/seller/addSingleItem';

const AddKeyModal = ({handleClose, successCallback, prodID, varID}) => {

    const [isMulti, setIsMulti] = useState(false);
    const [toggled, setToggled] = useState('l');

    const [keyContent, setKeyContent] = useState('');
    const [keysContent, setKeysContent] = useState([]);


      const isDesktop = useMediaQuery({minWidth: 1368});

    const createKey = async ()=>{
        AddSingleItem({
            content: keyContent
        },prodID,varID).then(res => console.log(res.split(' ')[1]));
        handleClose();
    };

    return (

                <div>
                    <p className={styles.title}>Добавление ключа</p>
                    <div style={{display:'flex', justifyContent:'center', marginBottom: 20}}>
                        {!isMulti ?
                            <div style={{display: 'flex'}}>
                                <FilledButton>
                                    Одиночное
                                </FilledButton>
                                <div style={{marginLeft: 10}}>
                                    <OutlinedButton
                                        color="primary"
                                        textColor="primary"
                                        onClick={() => {setIsMulti(!isMulti)}}>
                                        Мульти
                                    </OutlinedButton>
                                </div>

                            </div>
                            :
                            <div style={{display: 'flex'}}>
                                <OutlinedButton
                                    color="primary"
                                    textColor="primary"
                                    onClick={() => {setIsMulti(!isMulti);}}>
                                    Одиночное
                                </OutlinedButton>
                                <div style={{marginLeft: 10}}>
                                <FilledButton>
                                    Мульти
                                </FilledButton>
                                </div>
                            </div>
                        }

                    </div>

                    <div style={{display:'flex', justifyContent:'center'}}>
                        {!isMulti &&
                            <Input
                                onChange={(e) => {setKeyContent(e.target.value);}}
                                placeholder="Введите ключ" value={keyContent}/>
                        }
                        {
                            isMulti &&
                            <textarea name="" id="" cols="20" rows="10"
                                      style={{padding: 10, boxSizing: 'border-box', borderRadius: 12}} />
                        }

                    </div>

                    <div style={{display:'flex', justifyContent:'center', marginTop: 20}}>
                        <FilledButton onCLick={createKey}>
                            Добавить
                        </FilledButton>
                    </div>
                </div>
    );
};
export default AddKeyModal;