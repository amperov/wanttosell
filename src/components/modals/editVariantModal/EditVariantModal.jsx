import {useState} from 'react';
import styles from './EditVariantModal.module.scss';
import FilledButton from '../../uiKit/buttons/filledButton/FilledButton.jsx';

import {useMediaQuery} from 'react-responsive';
import {Text} from '../../uiKit/text/Text';
import Input from '../../uiKit/input/Input';

import {CreateVariant} from '../../../utils/api/requests/seller/variant/createVariant';

const EditVariantModal = ({handleClose, successCallback, variant, prodID}) => {
    const [engTitle, setEngTitle] = useState(variant.title.find(val => val.lang === 'en-US').value);
    const [ruTitle, setRuTitle] = useState(variant.title.find(val => val.lang === 'ru-RU').value);
    const [minCount, setMinCount] = useState(variant.min_count);
    const [price, setPrice] = useState(variant.price);



    const isDesktop = useMediaQuery({minWidth: 1368});

    const createVariant = async ()=>{
        const status = await EditVariant({
            title: [
                {
                    lang: 'en-US',
                    value: engTitle
                },
                {
                    lang: 'ru-RU',
                    value: ruTitle
                }
            ],
            price: parseFloat(price),
            min_count: parseInt(minCount),
        }, prodID, variant.id);
        handleClose();
    };

    return (
                <div>
                        <p className={styles.title}>Добавление варианта</p>

                        <div>
                            <Text fs={16} fw={400}>
                                Название:
                            </Text>
                            <div style={{marginLeft:40}}>
                                <Input placeholder="Введите название (RU)"
                                    value={ruTitle}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                    onChange={(e) => {setRuTitle(e.target.value);}}/>
                                <Input placeholder="Введите название (EN)"
                                       value={engTitle}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setEngTitle(e.target.value);}} />
                            </div>
                        </div>

                        <div>
                            <Text fs={16} fw={400}>
                                Прочие настройки:
                            </Text>
                            <div style={{marginLeft:40}}>
                                <Input placeholder="Введите мин. количество"
                                       value={minCount}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setMinCount(e.target.value);}}/>
                                <Input placeholder="Введите цену за штуку"
                                       value={price}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setPrice(e.target.value);}}/>
                        </div>

                        <div className={styles.actions}>
                            <FilledButton size="sm" rounded="sm" color="primary" onCLick={createVariant}>
                                Добавить
                            </FilledButton>
                        </div>
                    </div>
                </div>
    );
};
export default EditVariantModal;