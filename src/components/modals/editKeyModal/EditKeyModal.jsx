import {useState} from 'react';
import styles from './EditKeyModal.module.scss';
import FilledButton from '../../uiKit/buttons/filledButton/FilledButton.jsx';

import {useMediaQuery} from 'react-responsive';
import Input from '../../uiKit/input/Input';
import OutlinedButton from '../../uiKit/buttons/outlinedButton/OutlinedButton';
import {AddSingleItem} from '../../../utils/api/requests/seller/addSingleItem';
import {editItem} from '../../../utils/api/requests/seller/editItem';

const EditKeyModal = ({handleClose, successCallback, prodID, varID, item}) => {

    const [keyContent, setKeyContent] = useState(item.content);



    const isDesktop = useMediaQuery({minWidth: 1368});

    const updateKey = async ()=>{
        await editItem({
            content: keyContent
        }, prodID, varID, item.uuid);
        handleClose();
    };

    return (
                <div>
                    <p className={styles.title}>Обновление ключа</p>

                    <div style={{display:'flex', justifyContent:'center'}}>
                            <Input
                                onChange={(e) => {setKeyContent(e.target.value);}}
                                placeholder="Введите ключ" value={keyContent}/>
                    </div>
                    <div style={{display:'flex', justifyContent:'center', marginTop: 20}}>
                        <FilledButton onCLick={updateKey}>
                            Обновить
                        </FilledButton>
                    </div>
                </div>
    );
};
export default EditKeyModal;