import {useState} from 'react';
import styles from './EditProductModal.module.scss';
import FilledButton from '../../uiKit/buttons/filledButton/FilledButton.jsx';

import {useMediaQuery} from 'react-responsive';
import {Text} from '../../uiKit/text/Text';
import Input from '../../uiKit/input/Input';
import {EditProduct} from '../../../utils/api/requests/seller/product/editProduct';


export const EditProductModal = ({handleClose, successCallback, product}) => {

    const [ID, setID] = useState(product.id);

    const [engTitle, setEngTitle] = useState(product.title.find(val => val.lang === 'en-US').value);
    const [ruTitle, setRuTitle] = useState(product.title.find(val => val.lang === 'ru-RU').value);
    const [platform, setPlatform] = useState(product.platform);
    const [delivery, setDelivery] = useState(product.delivery);
    const [type, setType] = useState(product.type);
    const [os, setOS] = useState(product.os);

    const isDesktop = useMediaQuery({minWidth: 1368});

    const editProduct = async ()=>{
        const status = await EditProduct({
            title: [
                {
                    lang: 'en-US',
                    value: engTitle
                },
                {
                    lang: 'ru-RU',
                    value: ruTitle
                }
            ],
            delivery: delivery,
            type: type,
            os: os,
            description: 'А на фронте нема текстАреа'
        }, ID);
        console.log(`status: ${status}`);
        if (status){
            handleClose();
        }
    };

    return (

                <div>
                    <p className={styles.title}>Редактирование товара</p>
                    <div>
                        <div>
                            <Text fs={16} fw={400}>
                                Название:
                            </Text>
                            <div style={{marginLeft:40}}>
                                <Input placeholder="Введите название (RU)"
                                       value={ruTitle}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setRuTitle(e.target.value);}}/>
                                <Input placeholder="Введите название (EN)"
                                       value={engTitle}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setEngTitle(e.target.value);}} />
                            </div>
                        </div>

                        <div>
                            <Text fs={16} fw={400}>
                                Прочие настройки:
                            </Text>
                            <div style={{marginLeft:40}}>
                                <Input placeholder="Введите тип товара"
                                       value={type}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setType(e.target.value);}}/>
                                <Input placeholder="Введите платформу товара"
                                       value={platform}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setPlatform(e.target.value);}}/>
                                <Input placeholder="Введите тип доставки"
                                       value={delivery}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setDelivery(e.target.value);}} />
                                <Input placeholder="Введите операционную систему"
                                       value={os}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setOS(e.target.value);}} />
                            </div>
                        </div>
                    </div>

                    <div className={styles.actions}>

                            <FilledButton size="sm" rounded="sm" color="primary" onCLick={editProduct}>
                                Дальше
                            </FilledButton>



                    </div>
                </div>
    );
};
