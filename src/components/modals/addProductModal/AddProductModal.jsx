import {useState} from 'react';
import styles from './AddProductModal.module.scss';
import FilledButton from '../../uiKit/buttons/filledButton/FilledButton.jsx';

import {useMediaQuery} from 'react-responsive';
import {Text} from '../../uiKit/text/Text';
import Input from '../../uiKit/input/Input';
import CreateProduct from '../../../utils/api/requests/seller/product/createProduct';
import {UploadPhoto} from '../../../utils/api/requests/seller/uploadPhoto';

const AddProductModal = ({handleClose, successCallback}) => {
    const [step, setStep] = useState(1);

    const [productCreated, setProductCreated] = useState(false);
    const [createdID, setCreatedID] = useState(0);

    const [engTitle, setEngTitle] = useState('');
    const [ruTitle, setRuTitle] = useState('');

    const [engDesc, setEngDesc] = useState('');
    const [ruDesc, setRuDesc] = useState('');

    const [platform, setPlatform] = useState('');
    const [delivery, setDelivery] = useState('');
    const [type, setType] = useState('');
    const [os, setOS] = useState('');
    const [photo, setPhoto] = useState([]);


    const isDesktop = useMediaQuery({minWidth: 1368});

    const createProduct = async ()=>{
        const [status, id] = await CreateProduct({
            title: [
                {
                    lang: 'en-US',
                    value: engTitle
                },
                {
                    lang: 'ru-RU',
                    value: ruTitle
                }
            ],
            delivery: delivery,
            type: type,
            os: os,
            description: [
                {
                    lang: 'en-US',
                    value: engDesc
                },
                {
                    lang: 'ru-RU',
                    value: ruDesc
                }
            ],
            platform: platform,
        });
        setCreatedID(id);
        if (status){
           setProductCreated(true);
           setStep(3);
        }
    };

    const abortCreating = () => {
        //TODO
    };

    const handleFileSelect = (event) => {
        setPhoto(event.target.files);
    };
    const uploadPhoto = async () => {
      const data = new FormData();
        data.set('file',photo[0]);
        const status = await UploadPhoto(data,createdID);
        if (status === 201){
            handleClose();
        }
    };

    return (

                <div>
                    <p className={styles.title}>Добавление товара. Шаг {step}</p>

                    {step === 1 &&
                    <div>
                        <div>
                            <Text fs={16} fw={400}>
                                Название:
                            </Text>
                            <div style={{marginLeft:40}}>
                                <Input placeholder="Введите название (RU)"
                                       value={ruTitle}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setRuTitle(e.target.value);}}/>
                                <Input placeholder="Введите название (EN)"
                                       value={engTitle}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setEngTitle(e.target.value);}} />
                            </div>
                        </div>

                        <div>
                            <Text fs={16} fw={400}>
                                Прочие настройки:
                            </Text>
                            <div style={{marginLeft:40}}>
                                <Input placeholder="Введите тип товара"
                                       value={type}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setType(e.target.value);}}/>
                                <Input placeholder="Введите платформу товара"
                                       value={platform}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setPlatform(e.target.value);}}/>
                                <Input placeholder="Введите тип доставки"
                                       value={delivery}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setDelivery(e.target.value);}} />
                                <Input placeholder="Введите операционную систему"
                                       value={os}
                                       width={315}
                                       mt={10}
                                       mb={10}
                                       onChange={(e) => {setOS(e.target.value);}} />
                            </div>
                        </div>
                    </div>
                    }

                    {step === 2 &&

                        <div>
                            <div>
                                <Text fs={16} fw={400}>
                                    Описание:
                                </Text>
                                <div style={{marginLeft:40}}>
                                    <Input placeholder="Введите описание (RU)"
                                           value={ruDesc}
                                           width={315}
                                           mt={10}
                                           mb={10}
                                           onChange={(e) => {setRuDesc(e.target.value);}}/>
                                    <Input placeholder="Введите описание (EN)"
                                           value={engDesc}
                                           width={315}
                                           mt={10}
                                           mb={10}
                                           onChange={(e) => {setEngDesc(e.target.value);}} />
                                </div>
                            </div>

                        </div>
                    }



                    {
                        step === 3 &&
                        <div>
                            <input type="file" onChange={handleFileSelect}/>
                        </div>
                    }


                    <div className={styles.actions}>
                        { step === 1 &&
                            <FilledButton size="sm" rounded="sm" color="primary" onCLick={() => {setStep(2);}}>
                                Дальше
                            </FilledButton>
                        }

                        { step === 2 &&
                            <FilledButton size="sm" rounded="sm" color="primary" onCLick={createProduct}>
                                Дальше
                            </FilledButton>
                        }
                        {
                            step === 3 &&
                            <FilledButton size="sm" onCLick={uploadPhoto}>
                                Создать
                            </FilledButton>
                        }

                    </div>
                </div>
    );
};
export default AddProductModal;