import {useState} from 'react';
import styles from './ModalLogin.module.scss';
import Input from '../../uiKit/input/Input.jsx';
import FilledButton from '../../uiKit/buttons/filledButton/FilledButton.jsx';
import PasswordInput from '../../uiKit/passwordInput/PasswordInput.jsx';
import auth from '../../../utils/api/requests/auth.js';
import {useMediaQuery} from 'react-responsive';

const ModalLogin = ( {handleClose, successCallback}) => {
    const [isSignUp, setIsSignUp] = useState(false);
    const [login, setLogin] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const isDesktop = useMediaQuery({minWidth: 1368});
    const toSignUp = () => {
        setIsSignUp(true);
        nullForm();
    };
    const toSignIn = () => {
        setIsSignUp(false);
        nullForm();
    };
    const handleChangePassword = (e) => {
        setPassword(e.target.value);
    };
    const nullForm = () => {
        setPassword('');
        setUsername('');
        setEmail('');
        setFirstName('');
        setLastName('');

        setLogin('');
    };
    const SignUp = async () => {
        await auth({
            firstname: firstName,
            lastname: lastName,
            email: email,
            password: password,
            username: username
        }, 'sign-up', successCallback);
        nullForm();
        handleClose();
    };
    const SignIn = async () => {
        await auth({
            login: login,
            password: password
        }, 'sign-in', successCallback);
        handleClose();
    };

    return (
        <div>
            {isSignUp ?
                <div>
                    <p className={styles.title}>Регистрация</p>
                    <div>
                        <div style={{margin:'10px 0'}}>
                            <Input placeholder="Введите Имя" value={firstName} width={isDesktop ? 320 : 300}
                                   onChange={(e)=>{setFirstName(e.target.value);}}/>
                        </div>
                        <div style={{margin:'10px 0'}}>
                            <Input placeholder="Введите Фамилию" value={lastName} width={isDesktop ? 320 : 300}
                                   onChange={(e)=>{setLastName(e.target.value);}}/>
                        </div>
                        <div style={{margin:'10px 0'}}>
                            <Input placeholder="Введите Username" value={username} width={isDesktop ? 320 : 300}
                                   onChange={(e)=>{setUsername(e.target.value);}}/>
                        </div>
                        <div style={{margin:'10px 0'}}>
                            <Input placeholder="Введите Почту" value={email} width={isDesktop ? 320 : 300}
                                   onChange={(e)=>{setEmail(e.target.value);}}/>
                        </div>
                        <PasswordInput placeholder="Введите пароль" value={password} width={isDesktop ? 320 : 300}
                                       onChange={handleChangePassword} />
                    </div>
                    <div className={styles.actions}>
                        <button className={styles.buttonSublink} onClick={toSignIn}>
                            <p className={styles.textSublink}>
                                Есть аккаунт?
                            </p>
                        </button>
                        <FilledButton size="sm" rounded="sm" color="primary" onCLick={SignUp}>
                            Зарегистрироваться
                        </FilledButton>
                    </div>
                </div>
                :
                <div>
                    <p className={styles.title}>Авторизация</p>
                    <div>
                        <div style={{margin:'10px 0'}}>
                            <Input placeholder="Введите Логин" value={login} width={isDesktop ? 320 : 300}
                                   onChange={(e) => {setLogin(e.target.value)}}/>
                        </div>
                        <div style={{margin:'10px 0'}}>
                            <PasswordInput placeholder="Введите пароль"
                                           value={password}
                                           onChange={handleChangePassword}
                                           width={isDesktop ? 320 : 300}/>
                        </div>
                    </div>
                    <div className={styles.actions}>
                        <button className={styles.buttonSublink} onClick={toSignUp}>
                            <p className={styles.textSublink}>Нет аккаунта?</p>
                        </button>
                        <FilledButton size="sm" rounded="sm" color="primary" onCLick={SignIn}>
                            Авторизоваться
                        </FilledButton>
                    </div>
                </div>
            }
        </div>
    );
};
export default ModalLogin;