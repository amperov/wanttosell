import styles from './ModalLayout.module.scss';
import {FaRegWindowClose} from 'react-icons/fa';
import {useMediaQuery} from 'react-responsive';
import getClasses from '../../../utils/common/getClasses.js';
//ModalLayout get children, onCancel and Persist components
const ModalLayout = ({children, handleClose }) => {
    const isDesktop = useMediaQuery({minWidth: 1368});
    const isMobileOrTablet = useMediaQuery({maxWidth: 1368});
    return (
        <div className={styles.wrapper} >
                <div className={getClasses([[styles.layout, isDesktop], [styles.layoutMobile, isMobileOrTablet]])}>
                    <div style={{padding: 35}}>
                        {children}
                    </div>
                    <div style={{position:'absolute', top:10, right: 10}}>
                        <button style={{background:'none', border:'none'}} onClick={handleClose}><FaRegWindowClose size={32} color="#F0706A"/></button>
                    </div>
                </div>
        </div>
    );
};
export default ModalLayout;