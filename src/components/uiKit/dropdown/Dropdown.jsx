import {useState} from 'react';
import OutlinedButton from '../buttons/outlinedButton/OutlinedButton.jsx';
import {useMediaQuery} from 'react-responsive';
import {FaBars} from 'react-icons/fa';
import FilledButton from '../buttons/filledButton/FilledButton';

const Dropdown = ({children, size = "lg", color = 'white', textColor = 'white', text, variant = 'outlined'}) => {
    const [showMenu, setShowMenu] = useState(false);

    const isDesktop = useMediaQuery({minWidth: 1368});
    const changeVisibility = () =>{
        setShowMenu(!showMenu);
    };

    return (
        <div style={{position: 'relative'}}>
            {variant === 'outlined' ?
                <OutlinedButton size={size} color={color} textColor={textColor} onClick={changeVisibility}>
                    {text}
                </OutlinedButton>
                :
                <FilledButton size={size} color={color} textColor={textColor} onCLick={changeVisibility} rounded="sm">
                    {text}
                </FilledButton>
            }

            {showMenu && children}
        </div>
    );
};
export default Dropdown;