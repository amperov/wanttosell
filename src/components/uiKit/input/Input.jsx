import styles from './Input.module.scss';
const Input = ({width = 260, placeholder = '',children, type = 'text', value,mt = 0, mb = 0, onChange = (e) => {console.log(e.target.value)}}) => {

    return (
        <div style={{position: 'relative', display:'flex', alignItems: 'center',width, marginTop: mt, marginBottom: mb}}>
            <input type={type} className={styles.input}
                   style={{width}} placeholder={placeholder}
                   value={value} onChange={onChange}/>
            {children &&
                <div style={{position: 'absolute', bottom:3, right: 5, border: 'none', background: 'none'}}>
                    {children}
                </div>}

        </div>
    );
};

export default Input;