import getClasses from '../../../../utils/common/getClasses';
import styles from './SwitchButton.module.scss';
const switchButton = ({toggled = 'r', lValue, rValue, onToggle, mainColor = 'black'}) => {

    return (
        <div className={styles.container}>

                <button
                    className={getClasses([
                    styles.buttonLeft,
                    styles.button,
                    [styles.toggledButton, toggled === 'l'],
                    [styles.noToggledButton, toggled === 'r']])}
                onClick={onToggle}>
                    {lValue}
                </button>
                <button
                    className={getClasses([
                    styles.buttonRight,
                    styles.button,
                    [styles.toggledButton, toggled === 'r'],
                    [styles.noToggledButton, toggled === 'l']])}
                onClick={onToggle}>
                    {rValue}
                </button>
        </div>
    );
};

export default switchButton;