import getClasses from '../../../../utils/common/getClasses.js';
import styles from './FilledButton.module.scss';
const FilledButton = (
    {size = 'sm', rounded = 'sm', color = 'primary', textColor = 'white', children, text, onCLick}
) => {
    //TODO add Border/Background colors: blue, orange, red, purple
    //TODO add hovers for button
    return (
        <button className={getClasses(
            [
                styles.button,
                styles[`button_text__${textColor}`],
                styles[`button__${color}`],
                styles[`button__${size}`],
                styles[`button__rounded_${rounded}`]])}
        onClick={onCLick}>
            <div style={{ display: 'flex', alignItems: 'center'}}>
                {children}
            </div>

        </button>
    );

};
export default FilledButton;