import getClasses from '../../../../utils/common/getClasses.js';
import styles from './OutlinedButton.module.scss';
// eslint-disable-next-line react/prop-types
const OutlinedButton = ({size = 'sm', rounded = 'sm', color = 'success', textColor = 'white', onClick, children}) => {

    return (
        <button onClick={onClick}
            className={getClasses(
            [
                styles.button,
                styles[`button_text__${textColor}`],
                styles[`button__${color}`],
                styles[`button__${size}`],
                styles[`button__rounded_${rounded}`]])}>
            <div style={{ display: 'flex', alignItems: 'center', marginLeft: 0}}>
                {children}
            </div>

        </button>
    );

};
export default OutlinedButton;