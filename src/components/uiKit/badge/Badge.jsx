
export const Badge = ({children, br = 100}) => {
    return (
        <div style={{background: '#58B3F6', padding: '5px 10px', borderRadius:br}}>
            <p style={{color: 'white'}}>
                {children}
            </p>
        </div>
    );
};