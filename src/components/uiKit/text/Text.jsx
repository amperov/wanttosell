export const Text = ({prependIcon, endIcon, children, fs, fw, align = 'left'}) => {

    return (
        <div style={{display:'flex', alignItems: 'center'}}>
            {!!prependIcon && prependIcon}
            <p style={{fontSize:fs, fontWeight: fw, marginLeft: 10, marginRight: 10, textAlign: 'left'}}>{children}</p>
            {!!endIcon && endIcon}
        </div>
    );
};