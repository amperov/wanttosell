import {useEffect, useState} from 'react';
import {FaMinus, FaPlus} from 'react-icons/fa6';
import styles from './IncDecInput.module.scss';

export const IncDecInput = ({value = 5, onChange, minValue = 0, maxValue = 1500, isChosen}) => {
    const [inputValue, setInputValue] = useState(minValue);


    useEffect(() => {
        setInputValue(minValue);
    }, [minValue]);

    const insertValue = (e) => {
            setInputValue(parseInt(e.target.value));
            onChange(parseInt(e.target.value));
    };

    const onAbort = (e) => {
        if (parseInt(e.target.value) > maxValue){
            setInputValue(maxValue);
            onChange(maxValue);
        } else if (parseInt(e.target.value) < minValue){
            setInputValue(minValue);
            onChange(minValue);
        }
    };

    const decValue = () => {
        if ( inputValue - 1 >= minValue){
            onChange(inputValue - 1);
            setInputValue(inputValue - 1);
        } else {
            setInputValue(minValue);
        }
    };

    const incValue = () => {
        if (1 + inputValue <= maxValue){
            onChange(inputValue + 1);
            setInputValue(inputValue+1);
        } else {
            onChange(maxValue);
            setInputValue(maxValue);
        }
    };

    return (
        <div style={{ padding: '6px 5px', border: '2px solid lightgreen', display: 'flex', borderRadius: 12}}>
            <button onClick={decValue} className={styles.button} disabled={!isChosen}>
                <FaMinus color="white"/>
            </button>
            <input value={value} type="text" onChange={insertValue} onBlur={onAbort} disabled={!isChosen}
                className={styles.input}
            />
            <button onClick={incValue} className={styles.button} disabled={!isChosen}>
                <FaPlus color="white" />
            </button>
        </div>
    );

};