
export const TextArea = ({color, placeholder, onChange, value}) => {
    return (
        <div>
            <textarea name="" id="" cols="30" rows="10" style={{background: color, borderRadius: 12, padding: 8}}
                      placeholder={placeholder}
                      onChange={onChange}
                      value={value} />
        </div>
    );
};