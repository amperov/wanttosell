import OutlinedButton from '../UIKit/Buttons/OutlinedButton/OutlinedButton.jsx';
import ModalLayout from '../../layouts/modalLayout/ModalLayout.jsx';
import ModalLogin from '../UIKit/ modals/ModalLogin/ModalLogin.jsx';

const LoginButton = ({showModal, setShowModal, success}) => {

    return (
        <div>
            <OutlinedButton onCLick={setShowModal} textColor="white" color="white" size="md">Войти</OutlinedButton>
            {showModal &&
                <ModalLayout handleClose={setShowModal}>
                    <ModalLogin handleClose={setShowModal} successCallback={success}/>
                </ModalLayout>}
        </div>
    );
};
export default LoginButton;