import React from 'react';
import {Route, Routes} from 'react-router';
import {BrowserRouter} from 'react-router-dom';
import StorePage from './pages/storePage/StorePage.jsx';
import SellerProductList from './pages/sellerSide/sellerProductList/SellerProductList';
import {useMediaQuery} from 'react-responsive';
import {SellerProduct} from './pages/sellerSide/sellerProduct/SellerProduct';
import {SellerVariant} from './pages/sellerSide/sellerVariant/SellerVariant';
import {ClientProduct} from './pages/clientProduct/ClientProduct';

function App() {
    const isDesktop = useMediaQuery({minWidth: 1384});
  return (
      <div style={{position:'relative'}}>
          <BrowserRouter>
              <Routes>
                  <Route path="/store" element={<StorePage />}/>
                  <Route path="/store/:product_id" element={<ClientProduct />}/>
                  <Route path="/seller/products" element={<SellerProductList />}/>
                  <Route path="/seller/products/:product_id" element={<SellerProduct />}/>
                  <Route path="/seller/products/:product_id/variants/:variant_id" element={<SellerVariant />}/>
              </Routes>

          </BrowserRouter>
      </div>



  );
}

export default App;
