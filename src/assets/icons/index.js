import { ReactComponent as SimpleIcon } from './basic-refresh.svg';

import { ReactComponent as English } from './english.svg';
import {ReactComponent as Russian} from './russia-flag-icon.svg';
import {ReactComponent as XIcon} from './XIcon.svg';
import {ReactComponent as TelegramIcon} from './TelegramIcon.svg';
import {ReactComponent as VKIcon} from './VkIcon.svg';
import {ReactComponent as ArrowDown} from './arrowDown.svg';
import {ReactComponent as PlusIcon} from './Plus Button.svg';
import {ReactComponent as MinusIcon} from './Minus Button.svg';



export {SimpleIcon,  English, Russian, TelegramIcon, VKIcon, XIcon, ArrowDown, PlusIcon, MinusIcon};