import styles from './Header.module.scss';
const Header = ({children}) => {

    return (
        <div className={styles.wrapper}>
            <p style={{color: 'white', fontSize: 20}}>Каталог</p>
            <div className={styles.leftButton}>{children}</div>
        </div>
    );
};
export default Header;