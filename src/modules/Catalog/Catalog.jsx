import MiniProduct from '../../components/miniProduct/MiniProduct.jsx';
import styles from './Catalog.module.scss';
import {useMediaQuery} from 'react-responsive';
import getClasses from '../../utils/common/getClasses.js';
const Catalog = ({products = []}) => {
    const isDesktop = useMediaQuery({minWidth:1368});
    const isMobile = useMediaQuery({minWidth: 360, maxWidth: 768});
    const isTablet = useMediaQuery({minWidth: 768, maxWidth: 1368});
    return (
        <div className={getClasses([[styles.listDesktop, isDesktop], [styles.listMobile, isMobile], [styles.listTablet, isTablet]])}>
            {products.map( product => <MiniProduct key={product.id} product={product}/>)}
        </div>
    );
};
export default Catalog;