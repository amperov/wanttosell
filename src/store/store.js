import {configureStore} from '@reduxjs/toolkit';
import {reducer as LangReducer} from './settings/settings';

export const store = configureStore({
    reducer: {LangReducer}
});