import {createSlice} from '@reduxjs/toolkit';

 const localization = createSlice(
    {
        name: 'localization',
        initialState: {lang: 'en-US'},
        reducers: {
            changeLocale: (state, {payload: locale}) => {
                state.lang = locale;
            }
        }
    }
);

 export const {actions, reducer} = localization;