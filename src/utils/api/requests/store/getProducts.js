import Api from '../../Api.js';

const getProducts = async ()=> {
    let Resp  = await Api.GET('/products');
    console.log(Resp);
    return Resp.products;
};
export default getProducts;