
export  const GetSummaryCost = async (VariantID, Count) => {
  return new Intl.NumberFormat('ru').format(VariantID * Count);
};