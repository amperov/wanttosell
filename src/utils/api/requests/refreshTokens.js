import requestApi from '../Api.js';
import storage from '../utils/storage.js';
import BackendUrls from '../BackendUrls.js';
import setTokens from '../utils/setTokens.js';
const refreshTokens = async () => {
    const refreshToken = storage.GET('refresh');
    requestApi
        .POST(`${BackendUrls.auth}/refresh/`, {
            body: { refresh_token: refreshToken },
        })
        .then((res) => {
            setTokens(res);
            return 'Tokens updated successfully';
        });
};
export default refreshTokens;