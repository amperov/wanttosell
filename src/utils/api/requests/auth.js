import requestApi from '../Api.js';
import BackendUrls from '../BackendUrls.js';
import setTokens from '../utils/setTokens.js';
import jwtDecode from 'jwt-decode';
import storage from '../utils/storage.js';
const auth = async (body, type, collback) => {
    requestApi
        .POST(`${BackendUrls.auth}/${type}`, {
            body: body,
        })
        .then((res) => {
            setTokens(res);
            let decoded = jwtDecode(storage.GET('access'));
            storage.SET('user_role', decoded.user_role);
            collback();
        });

};
export default auth;