import Api from '../../../Api';
export const GetVariantsByProductID = async (productID) => {
    const Resp = await Api.GET(`/products/${productID}/variants`);
    console.log(Resp)
    return Resp;
};