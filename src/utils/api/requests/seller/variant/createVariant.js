import Api from '../../../Api';
export const CreateVariant = async (body = {}, ProdID) => {
    const Resp = await Api.POST(`/products/${ProdID}/variants`, {body: body});
    return Resp.status;
};