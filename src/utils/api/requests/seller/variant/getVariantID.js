import Api from '../../../Api';


export const GetVariantID = async (ProductID, VariantID) => {

    const Res = Api.GET(`/products/${ProductID}/variants/${VariantID}`);
    return Res;
};