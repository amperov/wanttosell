import Api from '../../Api';

export  const GetItems = async (ProductID, VariantID) => {

    const Res = await Api.GET(`/products/${ProductID}/variants/${VariantID}/items`)
    return Res.items;
};