import Api from '../../Api';

export const AddSingleItem = async (body, productID, variantID) => {
    const Resp = await Api.POST(`/products/${productID}/variants/${variantID}/items/single`, {body});
    return Resp;
};