import Api from '../../Api';
import storage from '../../utils/storage';
export const UploadPhoto = async (formData, productID) => {

    const token = storage.GET('access');
    const Res = await fetch(`https://wanttosell.online/api/products/${productID}/upload`,
        {body: formData, method: 'POST',
            headers: {
                'Authorization':`Bearer ${token}`,
                'Access-Control-Allow-Origin': '*'
        }});
    console.log(Res);
    return Res.status;
};