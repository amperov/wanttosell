import RequestApi from '../../../Api';
export const GetProductByID = async (id)=> {
    const Resp = await RequestApi.GET(`/products/${id}`);
    return Resp;
};