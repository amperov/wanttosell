import Api from '../../../Api.js';

const getOwnProducts = async ()=> {
    let Resp  = await Api.GET('/products/my');
    return Resp;
};
export default getOwnProducts;