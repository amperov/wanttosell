import requestApi from '../../../Api.js';

const CreateProduct = async (body) => {

    const Resp = await requestApi.POST('/products', {body:body});
    console.log(Resp);
    if (Resp.status_message === 'success'){
        return [true, Resp.id];
    }

};
export default CreateProduct;