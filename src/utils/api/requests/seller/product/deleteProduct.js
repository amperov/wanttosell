import Api from '../../../Api';
export const  DeleteProduct = (productID) => {
    Api.DELETE(`/products/${productID}`).then(res => console.log(res));
};