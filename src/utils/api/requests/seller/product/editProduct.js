import requestApi from '../../../Api.js';

export const EditProduct = async (body, id) => {

    const Resp = await requestApi.PATCH(`/products/${id}`, {body:body});
};
