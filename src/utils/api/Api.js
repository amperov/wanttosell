import storage from './utils/storage.js';
import URLs from './BackendUrls.js';
import checkOptions from './utils/checkOptions.js';
import refreshTokens from './requests/refreshTokens.js';

const BASE_URL = 'https://wanttosell.online/api';

class APIRequestError {
    constructor(message, details, status) {
        this.message = message;
        this.details = details;
        this.status = status;
        this.name = `Ошибка обращения к ${BASE_URL} API`;
    }
}



const makeRequest = async (requestUrl, fetchOptions, additionalHeaders) => {
    const isAuthentication = (requestUrl.includes(URLs.auth) || requestUrl.includes(URLs.auth));
    const authHeader = isAuthentication
        ? {}
        : (() => {
            const accessToken = storage.GET('access');
            if (!accessToken) return;
            return {
                ...additionalHeaders,
                Authorization: `Bearer ${accessToken}`,
            };
        })();

    const response = await fetch(`${BASE_URL}${requestUrl}`, {
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            ...additionalHeaders,
            ...authHeader,
        },
        ...fetchOptions,
    });

    if (response.status === 404) throw new APIRequestError('Сервис недоступен');
    if (response.status === 401) {
        if (!isAuthentication) {
            await refreshTokens();
            return await makeRequest(requestUrl, ...fetchOptions, ...additionalHeaders);
        }
    }
    const payload = response.status !== 204 ? await response.json() : undefined;
    if (!response.ok) throw new APIRequestError('Неизвестная ошибка', payload, response.status);
    return payload;
};

export default {
    GET: (path, fetchOptions) => {
        return makeRequest(path, {
            method: 'GET',
            ...checkOptions(fetchOptions),
        });
    },
    POST: (path, fetchOptions) => {
        return makeRequest(path, {
            method: 'POST',
            ...checkOptions(fetchOptions),
        });
    },
    PUT: (path, fetchOptions) => {
        return makeRequest(path, {
            method: 'PUT',
            ...checkOptions(fetchOptions),
        });
    },
    PATCH: (path, fetchOptions) => {
        return makeRequest(path, {
            method: 'PATCH',
            ...checkOptions(fetchOptions),
        });
    },
    DELETE: (path, fetchOptions) => {
        return makeRequest(path, {
            method: 'DELETE',
            ...checkOptions(fetchOptions),
        });
    },
};
