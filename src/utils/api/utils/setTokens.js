import storage from './storage.js';

const setTokens = (tokens) => {
    storage.SET('access', tokens.access_token, 'temp');
    storage.SET('refresh', tokens.refresh_token, 'const');
};

export default setTokens;
