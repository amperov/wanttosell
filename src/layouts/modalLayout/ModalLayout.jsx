import styles from './ModalLayout.module.scss';
import {FaRegWindowClose} from 'react-icons/fa';
//ModalLayout get children, onCancel and Persist components
const ModalLayout = ({children, handleClose }) => {
    return (
        <div className={styles.wrapper} >
                <div className={styles.layout}>
                    <div style={{padding: 35}}>
                        {children}
                    </div>
                    <div style={{position:'absolute', top:10, right: 10}}>
                        <button style={{background:'none', border:'none'}} onClick={handleClose}><FaRegWindowClose size={32} color="#F0706A"/></button>
                    </div>
                </div>
        </div>
    );
};
export default ModalLayout;